function busqueda(){

    let res = document.getElementById("lista");
    res.innerHTML="";

    const idBusqueda = document.getElementById("idBusqueda");
    const id = idBusqueda.value;

    if (isNaN(id) || id.trim() === "") {
        alert("Capture un número válido antes de realizar la búsqueda.");
        return;
    }

    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/users/" + id;

    //validar la respuesta

    http.onreadystatechange = function() {
        if (http.readyState == 4) {
            if (http.status == 200) {
               const datos = JSON.parse(this.responseText);
                res.innerHTML += '<tr> <td>' + datos.id + '</td>'
                + '<td>' + datos.name + '</td>'
                + '<td>' + datos.username + '</td>'
                + '<td>' + datos.email + '</td>'
                + '<td>' + datos.address.street + ", " + datos.address.city + '</td>'
                + '<td>' + datos.phone + '</td> </tr>'
            } else if (http.status == 404) {
                alert("ID incorrecta.");
            }
        }
    };
        http.open('GET',url,true);
        http.send();
}

//codificar los botones

document.getElementById("btnCargar").addEventListener("click",function(){
    busqueda();
});

document.getElementById("btnLimpiar").addEventListener("click",function(){
    let res = document.getElementById("lista");
    res.innerHTML="";
    document.getElementById("idBusqueda").value = "";
});
