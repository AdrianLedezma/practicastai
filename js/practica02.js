
function busqueda() {
    const idBusqueda = document.getElementById("idBusqueda");
    const id = idBusqueda.value;

    if (isNaN(id) || id.trim() === "") {
        alert("Capture un número válido antes de realizar la búsqueda.");
        return;
    }

    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums/" + id;


    http.onreadystatechange = function() {
        if (http.readyState == 4) {
            if (http.status == 200) {
                let resultado = document.getElementById("resultado");
                const datos = JSON.parse(http.responseText);
                resultado.textContent = datos.title;
            } else if (http.status == 404) {
                alert("ID incorrecta.");
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

//Codificación de los botones
document.getElementById("btnCargar").addEventListener("click",function(){
    busqueda();
});

document.getElementById("btnLimpiar").addEventListener("click",function(){
    let res = document.getElementById("resultado");
    res.textContent = ".";
    document.getElementById("idBusqueda").value = "";
});